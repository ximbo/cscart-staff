{assign var="staff" value=fn_staff_get_all()}
<div>
    <h3>{__("block_staff")}</h3>
    {if !empty($staff)}
        <div class="owl-carousel owl-theme" id="scroll_list_270" style="opacity: 1; display: block;">
            <div class="owl-wrapper-outer">
                <div class="owl-wrapper" style="width: 9405px; left: 0px; display: block;">
                {foreach from=$staff item=st}
                    <div class="owl-item" style="width: 171px;">
                        <div class="ty-center">
                            <h5>{$st.firstname} {$st.lastname}</h5>
                            <p>{$st.function}</p>
                            <em>{$st.email}</em>
                        </div>
                    </div>
                {/foreach}
                </div>
            </div>

            <div class="owl-controls clickable">
                <div class="owl-buttons">
                    <div class="owl-prev">Назад</div>
                    <div class="owl-next">Вперед</div>
                </div>
            </div>
        </div>
    {else}
        <p>{__("staff.empty")}</p>
    {/if}
</div>
