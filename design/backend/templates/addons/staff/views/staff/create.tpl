{capture name="mainbox"}
    <form action="" method="post" class="form-horizontal form-edit form-highlight" enctype="multipart/form-data">

        <div class="control-group">
            <label class="control-label">{__("staff.name")}</label>
            <div class="controls">
                <input class="span12" type="text" name="staff[firstname]">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">{__("staff.lastname")}</label>
            <div class="controls">
                <input class="span12" type="text" name="staff[lastname]">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">{__("staff.email")}</label>
            <div class="controls">
                <input class="span12" type="text" name="staff[email]">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label cm-required">{__("staff.function")}</label>
            <div class="controls">
                <textarea class="span12" name="staff[function]" required></textarea>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">{__("staff.photo")}</label>
            <div class="controls">
                <input class="span12" type="file" name="staff[photo]">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">{__("staff.user")}</label>
            <div class="controls">
                <input class="span2" type="number" name="staff[user_id]">
            </div>
        </div>

        <div class="control-group">
            <div class="controls">
                <input class="btn btn-default" type="reset" value="{__("staff.reset")}">
                <input class="btn btn-primary" type="submit" value="{__("staff.create")}">
            </div>
        </div>
    </form>
{/capture}

{include file="common/mainbox.tpl" title="{__("staff.createformtitle")}" content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}
