{capture name="mainbox"}
    {include file="common/pagination.tpl"}

    {capture name="buttons"}
        {include file="buttons/button.tpl" but_text='<i class="icon-plus"></i>' but_role="action" but_href="staff.create"}
    {/capture}


    <table class="table table-striped">
        <tr>
            <th>{__("staff.name")}</th><th>{__("staff.function")}</th><th>{__("staff.email")}</th><th>{__("staff.user")}</th><th></th>
        </tr>
        {foreach from=$staffs key=key item=staff}
            <tr>
                <td>{$staff.firstname|cat:" "|cat:$staff.lastname}</td>
                <td>{$staff.function}</td>
                <td>{$staff.email}</td>
                <td>{if $staff.user_id}<a href="{"profiles.update&user_id={$staff.user_id}"|fn_url}">{$staff.user_login} ({$staff.user_id})</a>{/if}</td>
                <td><a href="{"staff.update&staff_id={$staff.id}"|fn_url}">{__("staff.edit")}</a></td>
            </tr>
        {foreachelse}
            <tr><td>{__("staff.empty")}</td></tr>
        {/foreach}
    </table>
{/capture}

{include file="common/mainbox.tpl" title="{__("staff.formtitle")}" content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}