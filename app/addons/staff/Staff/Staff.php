<?php

namespace Staff;

class Staff
{

    /* @var Staff */
    protected static $instance;

    /* @var string Ключ данных формы */
    protected $key;

    /* @var string Плейсхолдер таблицы */
    protected $table = '?:staff';

    /* @var array Поля замены. Если staff.Поле пустое, то выбирается user.Поле */
    protected $fields = ['firstname', 'lastname', 'email'];

    public function __construct($key = 'staff') {
        $this->key = $key;
        self::$instance = self;
    }

    /**
     * @return Staff
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @return string
     */
    public function getKey() {
        return $this->key;
    }

    /**
     * Получение всех сотрудников
     *
     * @return array
     */
    public function all() {
        $fields = [];
        foreach ($this->fields as $field) $fields[] = "IF(`staff`.`{$field}`=\"\",`user`.`{$field}`,`staff`.`{$field}`) AS `{$field}`";
        $replace = implode(', ', $fields);
        $sql = "SELECT staff.*, user.user_login, {$replace} "
              ."FROM {$this->table} staff "
              ."LEFT JOIN ?:users user ON (staff.user_id = user.user_id) "
              ."ORDER BY staff.lastname"
        ;
        $data = db_get_array($sql);
        return $data;
    }

    /**
     * @param array $data
     */
    public function create($data = []) {
        db_query("INSERT INTO {$this->table} ?e", $data);
    }

    /**
     * @param array $data
     */
    public function update($data = []) {
        $id = $data['id'];
        unset($data['id']);
        db_query("UPDATE {$this->table} SET ?u WHERE `id` = ?i", $data, $id);
    }

    /**
     * @param array $data
     * @return array
     */
    public function delete($data = []) {
        return $data;
    }

    /**
     * Получение сотрудника по id
     *
     * @param int $id
     * @return array
     */
    public function get($id = 0) {
        $data = db_get_row("SELECT * FROM {$this->table} WHERE `id` = ?i", $id);
        return $data;
    }
}
