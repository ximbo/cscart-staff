<?php

$schema['staff'] = [
    'templates' => [
        'addons/staff/blocks/block.tpl' => [],
    ],
    'wrappers' => 'blocks/wrappers',
    'content' => [
        'staff' => [
            'type' => 'function',
            'function' => ['fn_staff_get_all'],
        ],
    ],
    'cache' => [
        'update_handlers' => [
            'product_features',
            'product_features_descriptions',
            'product_features_values',
            'product_feature_variants',
            'product_feature_variant_descriptions',
            'images_links'
        ],
    ],
];

return $schema;
