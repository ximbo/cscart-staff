<?php

use Tygh\Registry;
use Staff\Staff;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$staff = new Staff('staff');

if ($_SERVER['REQUEST_METHOD']	== 'POST') {

    $data = $_POST[$staff->getKey()];

    if ($mode == 'create') {
        $suffix = '.manage';
        // Обработка формы добавления нового сотрудника
        $staff->create($data);
    }

    if ($mode == 'update') {
        $suffix = '.manage';
        // Обработка формы обновления
        $staff->update($data);
    }

    if ($mode == 'delete') {
        // Do smth
        $suffix = '.manage';
    }

    return [CONTROLLER_STATUS_OK, $staff->getKey() . $suffix];
}

if ($mode == 'view') {

    // Do smth

} elseif ($mode == 'update') {

    // design/backend/templates/addons/staff/views/staff/update.tpl
    Tygh::$app['view']->assign([
        'staff' => $staff->get($_REQUEST['staff_id']),
    ]);

} elseif ($mode == 'create') {

    // design/backend/templates/addons/staff/views/staff/create.tpl

} elseif ($mode == 'manage' || $mode == 'picker') {

    // design/backend/templates/addons/staff/views/staff/manage.tpl
    Tygh::$app['view']->assign([
        'staffs' => $staff->all(),
    ]);

    /*
    $page_type = Tygh::$app['view']->getTemplateVars('page_type');
    if ($page_type == PAGE_TYPE_BLOG) {
        if (Registry::get('addons.discussion.status') == 'A') {
            Tygh::$app['view']->assign('discussion', array(
                'type' => 'C'
            ));
        }
    }
    */
    //return 'Alloha, baby';

}
