<?php

use Tygh\Registry;
use Tygh\Languages\Languages;
use Tygh\BlockManager\Block;
use Staff\Staff;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_staff_install() {
    return true;
}

function fn_staff_uninstall() {
    return true;
}

function fn_staff_get_all() {
    $staff = Staff::getInstance();
    return $staff->all();
}

if (fn_allowed_for('ULTIMATE')) {
    /*
     * function fn_smth() {
     * }
     */
}
